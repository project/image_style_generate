<?php

namespace Drupal\image_style_generate;

/**
 * A specific image style instance to import.
 */
class ImageStyleRow {

  /**
   * The data with which to generate the image style row.
   *
   * @var array
   */
  protected $data;

  /**
   * The ID (machine name) of the image style.
   *
   * @var string
   */
  protected $id;

  /**
   * The variables available for substitution.
   *
   * @var array
   */
  protected $variables = [];

  /**
   * Constructs an ImageStyleData object.
   *
   * @param array $data
   *   This argument should contain the following elements:
   *     - size_multiplier (optional)
   *     - id_pattern
   *     - label_pattern
   *     - group_id
   *     - group_label (optional)
   *     - base_size_dimension (optional)
   *     - aspect_ratio (optional)
   *     - base_size
   *     - size_scale
   *     - effects
   *   It will be used to generate the image style row.
   */
  public function __construct(array $data) {
    $this->data = $this->prepareData($data);
    $this->variables = $this->defineVariables();
    $this->id = $this->replaceVariables($this->data['id_pattern']);
  }

  /**
   * Generate a row of data for creating an image style.
   *
   * @return array
   *   The row of data for creating an image style.
   */
  public function getRow() {
    $row = [];

    // Set the name and label.
    $row['name'] = $this->id;
    $row['label'] = $this->replaceVariables($this->data['label_pattern']);

    // Add the effects.
    $weight = 0;
    foreach ($this->data['effects'] as $id => $data) {
      $data = $this->replaceVariables($data);
      $row['effects'][] = [
        'id' => $id,
        'weight' => $weight,
        'data' => $data,
      ];

      $weight++;
    }

    return $row;
  }

  /**
   * Get the ID to be used for the row.
   *
   * @return string
   *   The row ID.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Prepare the data.
   *
   * @param array $data
   *   The original data.
   *
   * @return array
   *   The prepared data.
   */
  protected function prepareData(array $data) {
    $data['size'] = $data['base_size'] * $data['size_scale'] / 100;

    // If the size multiplier is not specified, use 1.
    if (!isset($data['size_multiplier'])) {
      $data['size_multiplier'] = 1;
    }

    // If the base size dimension is not specified, assume it's the width.
    if (!isset($data['base_size_dimension'])) {
      $data['base_size_dimension'] = 'width';
    }

    // Calculate the width or height.
    if ($data['base_size_dimension'] == 'width') {
      $width = $data['size'] * $data['size_multiplier'];
    }
    else {
      $height = $data['size'] * $data['size_multiplier'];
    }

    // If the aspect ratio was defined, calculate the other dimension's value.
    if (isset($data['aspect_ratio'])) {
      list($aspect_ratio_width, $aspect_ratio_height) = explode(':', $data['aspect_ratio']);
      $data['aspect_ratio_width'] = $aspect_ratio_width;
      $data['aspect_ratio_height'] = $aspect_ratio_height;

      if ($data['base_size_dimension'] == 'width') {
        $height = round($width * $aspect_ratio_height / $aspect_ratio_width, 0);
      }
      else {
        $width = round($height * $aspect_ratio_width / $aspect_ratio_height, 0);
      }
    }
    else {
      $data['aspect_ratio'] = NULL;
      $data['aspect_ratio_width'] = NULL;
      $data['aspect_ratio_height'] = NULL;
    }

    $data['width'] = isset($width) ? $width : NULL;
    $data['height'] = isset($height) ? $height : NULL;

    return $data;
  }

  /**
   * Define the variables that can be used for substitution.
   *
   * @return array
   *   The available variables.
   */
  protected function defineVariables() {
    return [
      'size_multiplier' => $this->data['size_multiplier'],
      'group_id' => $this->data['group_id'],
      'group_label' => $this->data['group_label'],
      'base_size_dimension' => $this->data['base_size_dimension'],
      'aspect_ratio' => $this->data['aspect_ratio'],
      'aspect_ratio_width' => $this->data['aspect_ratio_width'],
      'aspect_ratio_height' => $this->data['aspect_ratio_height'],
      'base_size' => $this->data['base_size'],
      'base_size_padded' => $this->padValue($this->data['base_size']),
      'size' => $this->data['size'],
      'size_padded' => $this->padValue($this->data['size']),
      'width' => $this->data['width'],
      'height' => $this->data['height'],
      'size_scale' => $this->data['size_scale'],
      'size_scale_decimal' => $this->data['size_scale'] / 100,
    ];
  }

  /**
   * Recursively replace variables.
   *
   * @param array|string $data
   *   The data. If it is an array, parse it recursively.
   *
   * @return array
   *   The result of recursively replacing variables.
   */
  protected function replaceVariables($data) {
    if (is_array($data)) {
      foreach ($data as $key => $value) {
        $data[$key] = $this->replaceVariables($value);
      }
    }
    else {
      $replacements = [];
      foreach ($this->variables as $from => $to) {
        $replacements['{{' . $from . '}}'] = $to;
      }
      $data = strtr($data, $replacements);
    }

    return $data;
  }

  /**
   * Pad a value, such as a size id, to contain a specified number of digits.
   *
   * @param string $value
   *   The value to pad.
   * @param string $digits
   *   The number of digits.
   *
   * @return string
   *   The padded value.
   */
  protected function padValue($value, $digits = 6) {
    return str_pad(round($value * 1000, 0), $digits, '0', STR_PAD_LEFT);
  }

}
