CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Image Style Generate allows a site administrator to quickly create image styles
in bulk, based on a defined set of rules and patterns. It provides examples of
how migrations can employ the module's capabilities.


REQUIREMENTS
------------

* Migrate:
  Image Style Generate leverages the Migrate module in Drupal core to create
  image styles.

* Image:
  The Image Style entity and Image Effect plugin type are defined by the
  Image module in Drupal core.


RECOMMENDED MODULES
-------------------

 * Migrate Plus (https://www.drupal.org/project/migrate_plus):
   Enhances migrate functionality provided by Drupal core.

 * Migrate Tools (https://www.drupal.org/project/migrate_tools):
   Adds a UI and Drush commands for executing migrations.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration of the module is required. See the provided example
migrations for suggestions of how to customize this module's functionality
in a migration configuration file or see the project's page on Drupal.org
(https://www.drupal.org/project/image_style_generate) for more information.


MAINTAINERS
-----------

Current maintainers:
 * Rick Hawkins (rlhawk) - https://www.drupal.org/u/rlhawk
